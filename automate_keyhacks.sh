#!/bin/bash

# Automating testing of api-keys and access-tokens

# source: https://github.com/streaak/keyhacks

function usage(){
    echo "Usage: $0 <token/api-key>"
    echo
    exit
}

if [ $# -ne 1 ] ; then
    usage
fi

if [ $# -eq 1 ] ; then
    cmd=""   
    cmd="curl -H 'Authorization: Bearer $1' 'https://api.buildkite.com/v1/user'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl -X GET -H 'Content-Type: application/json' -H 'Authorization: Bearer $1' 'https://aum.iris.comcast.net/v1.1/user/current'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl 'https://www.deviantart.com/api/v1/oauth1/placebo' -d 'access_token=$1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl -H 'Authorization: Bearer $1' 'https://app.asana.com/api/1.0/users/me'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl -X POST 'https://api.dropboxapi.com/1/users/get_current_account' --header 'Authorization: Bearer $1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl -s -H 'Authorization: Bearer $1' 'https://api.github.com/search/code?sort=indexed&order=desc&q=osefdtc'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl 'https://maps.googleapis.com/maps/api/directions/json?origin=Toronto&destination=Montreal&key=$1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl 'https://maps.googleapis.com/maps/api/streetview?size=400x400&location=40.720032,-73.988354&fov=90&heading=235&pitch=10&key=$1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl 'https://maps.googleapis.com/maps/api/geocode/json?latlng=40,30&key=$1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl -X POST 'https://www.googleapis.com/geolocation/v1/geolocate?key=$1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo

    cmd="curl 'https://maps.googleapis.com/maps/api/timezone/json?location=39.6034810,-119.6822510&timestamp=1331161200&key=$1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl 'https://maps.googleapis.com/maps/api/staticmap?center=45%2C10&zoom=7&size=400x400&key=$1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl 'https://www.google.com/maps/embed/v1/place?q=place_id:ChIJyX7muQw8tokR2Vf5WBBk1iQ&key=$1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl -X POST 'https://api.heroku.com/apps' -H 'Accept: application/vnd.heroku+json; version=3' -H 'Authorization: Bearer $1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl --user 'api:key-$1' 'https://api.mailgun.net/v3/domains'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl 'https://api.mapbox.com/geocoding/v5/mapbox.places/Los%10Angeles.json?access_token=$1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl -H 'Accept: application/vnd.pagerduty+json;version=1' -H 'Authorization: Token token=$1' -X GET 'https://api.pagerduty.com/schedules'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl -X POST https://api-m.sandbox.paypal.com/v2/checkout/orders -H 'Content-Type: application/json' -H 'Authorization: Bearer $1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl -X GET 'https://api.sendgrid.com/v3/scopes' -H 'Authorization: Bearer $1' -H 'Content-Type: application/json'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl -sX POST 'https://slack.com/api/auth.test?token=$1&pretty=1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl 'https://connect.squareup.com/v1/locations' -H 'Authorization: Bearer $1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl 'https://api.stripe.com/v1/' -u '$1:'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl -H 'Travis-API-Version: 3' -H 'Authorization: token $1' 'https://api.travis-ci.com/user'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl --request GET --url 'https://api.twitter.com/1.1/account_activity/all/subscriptions/count.json' --header 'authorization: Bearer $1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl -H 'Authorization: Bearer $1' 'https://api.spotify.com/v1/me'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl 'https://wakatime.com/api/v1/users/current/projects/?api_key=$1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl -H 'Content-Type: application/json' -H 'Authorization: Bearer $1'  'https://sandbox.wompi.co/v1/pse/financial_institutions'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="curl -H 'Content-Type: application/json' -H 'Authorization: Bearer $1'  'https://production.wompi.co/v1/pse/financial_institutions'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="firefox 'https://developers.facebook.com/tools/debug/accesstoken/?access_token=$1&version=v3.1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="firefox 'https://api-ssl.bitly.com/v3/shorten?access_token=$1&longUrl=https://www.google.com' "
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
    cmd="firefox 'https://api.instagram.com/v1/users/self/?access_token=$1'"
    echo $cmd
    echo
    eval $cmd
    echo
    echo "----------------------------------------------------------------------"
    echo
else
    usage
    echo
fi

