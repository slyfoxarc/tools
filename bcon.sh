#!/bin/bash

# Connecting to a paired device from the command line

#turning on Bluetooth

rfkill unblock bluetooth
#power on bluetoothctl

bluetoothctl power on


#getting MAC address of paired devices

declare -a device_list mac_list 
readarray -t device_list < <(bluetoothctl paired-devices)

c=1
d=0

echo -e "List of paired devices :\n"

for i in "${device_list[@]}"
do

	output=$(echo "${i}" | cut -d " " -f 3- )
	echo -e "${c}. ${output}\n"
	c=$(expr ${c} + 1 )
	mac_list[${d}]=$(echo "${i}" | cut -d " " -f 2 )
	d=$(expr ${d} + 1 )

done



#choosing which device to connect to

if [[ -z ${1} ]] 
then

	echo -e "Enter the device number to connect to : "

	read choice
	choice=$(expr ${choice} - 1 )
	
	if [[ ${choice} > ${d} && ${choice} < 1 ]]
	then
		echo "Wrong choice"
	fi

else 
	
	if [[ ${1} -le ${d} && ${1} -ge 1 ]]
	then
		
		choice=$(expr ${1} - 1 )
	else 
		echo "Wrong choice"
	fi

	
fi



#connecting 

op=$(bluetoothctl connect ${mac_list[${choice}]})

op=$(echo "${op}" | grep -i failed)

if [ -z "${op}" ]
then 
	echo "Connection Successful"
else 
	
	echo "Connection Failed"

fi



# troubleshooting : 

# 1. bluetoothctl 
# 2. sometimes computer might not be able to detect the device
# 3. you might have to untrust , remove the device 
# 4. re-pair the device 
# 5. bluetoothctl devices ---> scanned devices
# 6. bluetoothctl paired-devices ---> paired devices
